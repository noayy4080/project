@extends('layouts.app')

@section('title', 'Candidates')

@section('content')

<h1>List of users</h1>

<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
            <td>{{$user->id}}</td>
            <td><a href="{{route('candidate.userCan', $user->id)}}">{{$user->name}}</a></td>
        </tr>
    @endforeach
</table>
@endsection

